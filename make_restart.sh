#!/bin/bash

python3 inject_tiles/inject_tiles.py \
        -i data/inputs/pre-industrial.astart \
        -f data/outputs/1850_ACCESS-ESM_frac_IGBP_with_new_pft_in_Aus.nc \
        -n fraction \
        -o data/outputs/um_restarts/pre-industrial_newpft.astart

python3 inject_tiles/inject_tiles.py \
        -i data/inputs/pre-industrial.astart \
        -f data/outputs/1850_ACCESS-ESM_frac_IGBP_with_NVIS_in_Aus.nc \
        -n fraction \
        -o data/outputs/um_restarts/pre-industrial_nvis.astart
