#!/usr/bin/env python3
"""Update ACCESS_restart file with CABLE tiled fields with new tiles.
If tiles are not initialised then they will be initialised with
a latitude average for that tile.

Load the umfile_utils library with

deactivate
module use ~access/modules
module purge
module load pythonlib/umfile_utils
source ~/envs/dev/bin/activate
"""
import argparse
import sys

import netCDF4 as nc
import numpy as np
from um_fileheaders import *
import umfile


TILED_CODES = [
        216, 224, 227, 228, 229, 230, 233, 234, 236, 237, 240, 801, 802, 803, 804, 805, 806,
        807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 822, 823, 824,
        825, 826, 827, 828, 829, 830, 831, 832, 833, 835, 851, 852, 853, 854, 855, 856, 857,
        858, 859, 860, 861, 862, 863, 864, 865, 866, 867, 868, 869, 870, 871, 872, 873, 874,
        875, 876, 877, 878, 879, 880, 881, 882, 893, 894, 895, 896, 897, 898, 899, 900, 901,
        902, 903, 904, 905, 906, 916, 3173, 3287, 3288, 3314, 3317, 3321, 3331, 3801, 3802,
        3803, 3804, 3805, 3806, 3807, 3808, 3809, 3810, 3811, 3812, 3813, 3814, 3815, 3816,
        3817, 3818, 3819, 3820, 3821, 3822, 3823, 3824, 3825, 3826, 3827, 3828, 3829, 3830,
        3831, 3832, 3884, 3885, 3893,
        ]
MASK_CODE = 30
NTILES = 17
BLANK_PFT = 12 - 1
CROP_PFT = 9 - 1
FRACTIONS_CODE = 216
PREVIOUS_YEAR_FRACTIONS_CODE = 835
READ_ONLY = 'r'
WRITE_ONLY = 'w'
CABLE_TILES = list(range(17))
MISSING = -1073741824.0


def read_tiles(infile:umfile.UMFile, field_no:int, code:int)->np.ndarray:
    """Read a tiled variable for a given code in a UM dump file.

    Arguments:
        - infile : input UM restart dump file.
        - field_no : the field number of the first tile for the desired code.
        - code : the code the field should correspond to.
    """
    fields_list = []
    for i in range(0, NTILES):
        if infile.ilookup[field_no+i][ITEM_CODE] != code:
            sys.exit(f"Missing tiled fields {field_no} {code}")
        fields_list.append(infile.readfld(field_no+i))
    return np.array(fields_list)


# Parse command line arguments
parser = argparse.ArgumentParser(description="Update new tiles")
parser.add_argument('-i', '--input',  dest='ifile', help='Input UM dump')
parser.add_argument('-f', '--fractions',  dest='fracfile', help='Input NetCDF cover fractions')
parser.add_argument('-n', '--name',  dest='var_name', help='Fractions variable name')
parser.add_argument('-o', '--output', dest='ofile', help='Output UM dump')
parser.add_argument('-v', '--verbose',
        dest='verbose',
        action='store_true',
        default=False,
        help='verbose output',
        )
args = parser.parse_args()

# Open input file
input_file = umfile.UMFile(args.ifile)
nfields = input_file.fixhd[FH_LookupSize2]

ncfile = nc.Dataset(args.fracfile, READ_ONLY)
new_fractions = ncfile.variables[args.var_name][:].squeeze()
ncfile.close()

# open output file
output_file = umfile.UMFile(args.ofile, WRITE_ONLY)
output_file.copyheader(input_file)

# Read the file first to get the tiled data
tile_data = {}
field_no = 0
while field_no<nfields:
    ilookup = input_file.ilookup[field_no]
    if ilookup[LBEGIN]==-99: break # End of restart file.
    code = ilookup[ITEM_CODE]
    if code in TILED_CODES:
        if args.verbose: print("Reading code: ", code)
        tile_data[code] = read_tiles(input_file, field_no, ilookup[ITEM_CODE])
        field_no += NTILES
    else:
        field_no += 1

# Save 0s.
zeros = tile_data[FRACTIONS_CODE][BLANK_PFT].copy()

# Insert new fractions into tile_data
tile_data[FRACTIONS_CODE] = new_fractions

# Find new tiles.
old_doesnt_exist = tile_data[PREVIOUS_YEAR_FRACTIONS_CODE]==0.0
new_tiles = (new_fractions>0.0) & old_doesnt_exist

# Initialise new tiles.
ZERO_CODES = [
        229, #SOIL MOISTURE FRACTION
        240, #RUNNOFF
        813, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 1
        814, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 2
        815, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 3
        816, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 4
        817, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 5
        818, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 6
        819, #CABLE SNOW DEPTH ON TILES LAYER 1
        820, #CABLE SNOW DEPTH ON TILES LAYER 2
        821, #CABLE SNOW DEPTH ON TILES LAYER 3
        822, #CABLE SNOW MASS ON TILES LAYER 1
        823, #CABLE SNOW MASS ON TILES LAYER 2
        824, #CABLE SNOW MASS ON TILES LAYER 3
        828, #CABLE SNOW DENSITY ON TILES LAYER 1
        829, #CABLE SNOW DENSITY ON TILES LAYER 2
        830, #CABLE SNOW DENSITY ON TILES LAYER 3
        831, #CABLE MEAN SNOW DENSITY ON TILES
        832, #CABLE SNOW AGE ON TILES
        835, #PREVIOUS YEAR SURF FRACTIONS (TILES)
        851, #CARBON POOL LABILE ON TILES
        861, #NITROGEN POOL PLANT - LEAF ON TILES
        862, #NITROGEN POOL PLANT - WOOD ON TILES
        863, #NITROGEN POOL PLANT - ROOT ON TILES
        864, #NITROGEN POOL LITTER - METB ON TILES
        865, #NITROGEN POOL LITTER - STR ON TILES
        866, #NITROGEN POOL LITTER - CWD ON TILES
        867, #NITROGEN POOL SOIL - MIC ON TILES
        868, #NITROGEN POOL SOIL - SLOW ON TILES
        869, #NITROGEN POOL SOIL - PASS ON TILES
        870, #NITROGEN POOL SOIL MINIMUM (TILES)
        871, #PHOSPHORUS POOL PLANT - LEAF (TILES)
        872, #PHOSPHORUS POOL PLANT - WOOD (TILES)
        873, #PHOSPHORUS POOL PLANT- ROOT (TILES)
        874, #PHOSPHORUS POOL LITTER - METB (TILES)
        875, #PHOSPHORUS POOL LITTER - STR (TILES)
        876, #PHOSPHORUS POOL LITTER - CWD (TILES)
        877, #PHOSPHORUS POOL SOIL - MIC (TILES)
        878, #PHOSPHORUS POOL SOIL - SLOW (TILES)
        879, #PHOSPHORUS POOL SOIL - PASS (TILES)
        880, #PHOSPHORUS POOL SOIL LABILE (TILES)
        881, #PHOSPHORUS POOL SOIL SORB ON TILES
        882, #PHOSPHORUS POOL SOIL OCC ON TILES
        895, #WOOD FLUX CARBON (CASA-CNP)
        896, #WOOD FLUX NITROGEN (CASA-CNP)
        897, #WOOD FLUX PHOSPHOR (CASA-CNP)
        898, #WOOD HARVEST CARBON1(CASA-CNP)
        899, #WOOD HARVEST CARBON2(CASA-CNP)
        900, #WOOD HARVEST CARBON3(CASA-CNP)
        901, #WOOD HARVEST NITROG1(CASA-CNP)
        902, #WOOD HARVEST NITROG2(CASA-CNP)
        903, #WOOD HARVEST NITROG3(CASA-CNP)
        904, #WOOD HARVEST PHOSPH1(CASA-CNP)
        905, #WOOD HARVEST PHOSPH2(CASA-CNP)
        906, #WOOD HARVEST PHOSPH3(CASA-CNP)
        916, #WOOD RESPIRA CARBON1(CASA-CNP)
        3813, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 1
        3814, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 2
        3815, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 3
        3816, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 4
        3817, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 5
        3818, #CABLE FROZEN SOIL MOIST FRAC ON TILES LAYER 6
        3819, #CABLE SNOW DEPTH ON TILES LAYER 1
        3820, #CABLE SNOW DEPTH ON TILES LAYER 2
        3821, #CABLE SNOW DEPTH ON TILES LAYER 3
        3822, #CABLE SNOW MASS ON TILES LAYER 1
        3823, #CABLE SNOW MASS ON TILES LAYER 2
        3824, #CABLE SNOW MASS ON TILES LAYER 3
        3828, #CABLE SNOW DENSITY ON TILES LAYER 1
        3829, #CABLE SNOW DENSITY ON TILES LAYER 2
        3830, #CABLE SNOW DENSITY ON TILES LAYER 3
        3831, #CABLE MEAN SNOW DENSITY ON TILES
        3832, #CABLE SNOW AGE ON TILES
        3884, #NITROGEN DEPOSITION
        3885, #NITROGEN FIXATION
        ]
for code in TILED_CODES:
    if code in ZERO_CODES: continue
    if code==PREVIOUS_YEAR_FRACTIONS_CODE or code==FRACTIONS_CODE: continue
    if not code in tile_data.keys():
        print("skipping", code, ". It wasn't in the restart file.")
        continue
    temp = tile_data[code].copy()
    if temp.dtype==np.float64:
        temp[temp==MISSING] = np.nan
        temp[temp==0] = np.nan
        # Find australia mean
        for tile in CABLE_TILES:
            if not new_tiles[tile].any(): continue
            if tile==11:
                try:
                    tile_data[code][tile,new_tiles[tile]] = np.nanmean(temp[4,36:62,59:83], axis=(-1,-2))
                except:
                    tile_data[code][tile,new_tiles[tile]] = 0.0
            elif tile==12:
                try:
                    tile_data[code][tile,new_tiles[tile]] = np.nanmean(temp[0,36:62,59:83], axis=(-1,-2))
                except:
                    tile_data[code][tile,new_tiles[tile]] = 0.0
            else:
                try:
                    tile_data[code][tile,new_tiles[tile]] = np.nanmean(temp[tile,36:62,59:83], axis=(-1,-2))
                except:
                    tile_data[code][tile,new_tiles[tile]] = 0.0
    else:
        temp = np.float64(temp)
        print(code)
        temp[temp==MISSING] = np.nan
        for tile in CABLE_TILES:
            # Find australia median
            if tile==11:
                tile_data[code][tile,new_tiles[tile]] = np.int64(np.nanmedian(temp[4,36:62,59:83], axis=(-1,-2)))
            elif tile==12:
                tile_data[code][tile,new_tiles[tile]] = np.int64(np.nanmedian(temp[0,36:62,59:83], axis=(-1,-2)))
            else:
                tile_data[code][tile,new_tiles[tile]] = np.int64(np.nanmedian(temp[tile,36:62,59:83], axis=(-1,-2)))

for code in ZERO_CODES:
    if not code in tile_data.keys():
        print("skipping", code, ". It wasn't in the restart file.")
        continue
    for tile in CABLE_TILES:
        if not new_tiles[tile].any(): continue
        tile_data[code][tile,new_tiles[tile]] = 0

# Write to file
field_no = 0
while field_no<nfields:
    ilookup = input_file.ilookup[field_no]
    lbegin = ilookup[LBEGIN]
    if lbegin==-99:
        break
    code = ilookup[ITEM_CODE]

    if code in TILED_CODES:
        # Write the tiled output
        for i in range(NTILES):
            output_file.writefld(tile_data[code][i], field_no+i)
        field_no += NTILES
    else:
        if code==MASK_CODE:
            # Save the mask (needed for compression)
            field = input_file.readfld(field_no)
            output_file.writefld(field, field_no)
            output_file.mask = field
        else:
            # Copy from input file
            field = input_file.readfld(field_no, raw=True)
            output_file.writefld(field, field_no, raw=True)
        field_no += 1

output_file.close()

