#!/usr/bin/env python3
"""Insert the NVIS Australia potential vegetation map in the old global potential
vegetation map for ACCESS-ESM1.5 taken from CMIP6.
"""
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np

AUS_NVIS_ACCESS_POT_VEG_FILE = '/g/data/p66/tfl561/NVIS/data/NVIS_ACCESS_fractions.nc'
AUS_NVIS_ACCESS_NEW_PFT_FILE = '/g/data/p66/tfl561/NVIS/data/NVIS_ACCESS_new_pft_fractions.nc'
#GLOBAL_POT_VEG_FILE = '/g/data/p66/txz599/data/luc_hist_maps/cableCMIP6_LC_1850.nc'
GLOBAL_POT_VEG_FILE = 'data/inputs/cableCMIP6_LC_1850.nc'
READ_ONLY = 'r'
WRITE_ONLY = 'w'
STANDARD_TILE_NAMES = [
        'needle_leaf_evergreen_trees',
        'broad_leaf_evergreen_trees',
        'needle_leaf_deciduous_trees',
        'broad_leaf_deciduous_trees',
        'shrubs',
        'natural_grasses_of_c3_plant_functional_types',
        'natural_grasses_of_c4_plant_functional_types',
        'tundra',
        'crops_of_c3_plant_functional_types',
        'crops_of_c4_plant_functional_types',
        'wetland',
        'unused',
        'unused',
        'bare_ground',
        'urban',
        'fresh_free_water',
        'ice_on_land',
        ]

# Load the Australia NVIS potential vegetation map for ACCESS.
ncfile = nc.Dataset(AUS_NVIS_ACCESS_POT_VEG_FILE, READ_ONLY)
aus_nvis_frac = ncfile.variables['fraction'][:].squeeze()
aus_lats = ncfile.variables['lat'][:]
aus_lons = ncfile.variables['lon'][:]
ncfile.close()

# Load the Australia NVIS potential vegetation map for ACCESS.
ncfile = nc.Dataset(AUS_NVIS_ACCESS_NEW_PFT_FILE, READ_ONLY)
aus_nvis_new_pft_frac = ncfile.variables['fraction'][:].squeeze()
ncfile.close()

# Load the global IGPB potential vegetation map for ACCESS.
ncfile = nc.Dataset(GLOBAL_POT_VEG_FILE, READ_ONLY)
ACCESS_frac = ncfile.variables['fraction'][:].squeeze()
ACCESS_new_pft_frac = ACCESS_frac.copy()
lats = ncfile.variables['latitude'][:]
lons = ncfile.variables['longitude'][:]
NTILES = ncfile.dimensions['vegtype'].size
ncfile.close()

# Find where Australian indices are
y = np.where(lats==aus_lats[0])[0][0]
x = np.where(lons==aus_lons[0])[0][0]

# Insert the Australian map into the Global map
ACCESS_frac[:,y:y+len(aus_lats),x:x+len(aus_lons)] = aus_nvis_frac

# Insert the Australian map into the Global map with new PFTs
ACCESS_new_pft_frac[:,y:y+len(aus_lats),x:x+len(aus_lons)] = aus_nvis_new_pft_frac


def save_netcdf(filename: str, fracs):
    """Save cover fraction to a netCDF file.
    """
    ncout = nc.Dataset(filename, WRITE_ONLY)
    ncout.createDimension('time', size=None)
    ncout.createDimension('tile', size=NTILES)
    ncout.createDimension('string80', size=80)
    ncout.createDimension('lat', size=len(lats))
    ncout.createDimension('lon', size=len(lons))
    time = ncout.createVariable('time', 'double', dimensions=('time'))
    time.setncattr('units', 'days since 1850-01-01 00:00:00')
    time.setncattr('standard_name', 'time')
    time.setncattr('calendar', 'proleptic_gregorian')
    time.setncattr('axis', 'T')
    time.setncattr('cell_methods', 'time: mean')
    tile = ncout.createVariable('tile', 'c', dimensions=('tile','string80'))
    tile.setncattr('standard_name', 'area_type')
    lat = ncout.createVariable('lat', 'double', dimensions=('lat'))
    lat.setncattr('units', 'degrees_north')
    lat.setncattr('standard_name', 'latitude')
    lat.setncattr('point_spacing', 'even')
    lat.setncattr('axis', 'Y')
    lat.setncattr('long_name', 'latitudes at V grid points')
    lon = ncout.createVariable('lon', 'double', dimensions=('lon'))
    lon.setncattr('units', 'degrees_east')
    lon.setncattr('standard_name', 'longitude')
    lon.setncattr('point_spacing', 'even')
    lon.setncattr('axis', 'X')
    lon.setncattr('long_name', 'longitudes at U grid points')
    fraction = ncout.createVariable(
            'fraction',
            'float',
            dimensions=('time','tile','lat','lon'),
            fill_value=-1.073742e+09,
            )
    fraction.setncattr('standard_name', 'area_fraction')
    fraction.setncattr('long_name', "Surface tile fractions")
    fraction.setncattr('missing_value', -1.073742e+09)

    ncout.setncattr('script', f'created by {__file__}')
    ncout.setncattr('git', 'https://gitlab.com/tammasloughran/new-pft.git')
    ncout.setncattr('contact', 'Tammas Loughran tamloughran@outlook.com')

    time[:] = [0]
    for i in range(NTILES):
        l = len(STANDARD_TILE_NAMES[i])
        tile[i,0:l] = STANDARD_TILE_NAMES[i]
    lat[:] = lats
    lon[:] = lons
    fraction[0] = fracs

    ncfile.close()


# Save to NetCDF file.
save_netcdf(
        'data/outputs/1850_ACCESS-ESM_frac_IGBP_with_NVIS_in_Aus.nc',
        ACCESS_frac,
        )
save_netcdf(
        'data/outputs/1850_ACCESS-ESM_frac_IGBP_with_new_pft_in_Aus.nc',
        ACCESS_new_pft_frac,
        )
