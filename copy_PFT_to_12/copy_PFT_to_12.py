#!/usr/bin/env python3
"""Update CABLE tiled fields with new plant functional type.
Load the umfile_utils library with

deactivate
module use ~access/modules
module purge
module load pythonlib/umfile_utils
source ~/envs/dev/bin/activate
"""
import argparse
import sys

import netCDF4
import numpy as np
from um_fileheaders import *

import umfile

TILED_CODES = [
        216, 224, 227, 228, 229, 230, 233, 234, 236, 237, 240, 801, 802, 803, 804, 805, 806, 807, 808, 809,
        810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820, 822, 823, 824, 825, 826, 827, 828, 829, 830,
        831, 832, 833, 835, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864, 865, 866,
        867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 893, 894, 895, 896,
        897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 916, 3173, 3287, 3288, 3314, 3317, 3321, 3331,
        3801, 3802, 3803, 3804, 3805, 3806, 3807, 3808, 3809, 3810, 3811, 3812, 3813, 3814, 3815, 3816,
        3817, 3818, 3819, 3820, 3821, 3822, 3823, 3824, 3825, 3826, 3827, 3828, 3829, 3830, 3831, 3832,
        3884, 3885, 3893,
        ]
MASK_CODE = 30
NTILES = 17
BLANK_PFT = 12 - 1
CROP_PFT = 9 - 1
FRACTIONS_CODE = 216
PREVIOUS_YEAR_FRACTIONS_CODE = 835


def read_tiles(infile:umfile.UMFile, field_no:int, code:int)->np.ndarray:
    """Read a tiled variable for a given code in a UM dump file.

    Arguments:
        - infile : input UM restart dump file.
        - field_no : the field number of the first tile for the desired code.
        - code : the code the field should correspond to.
    """
    fields_list = []
    for i in range(0, NTILES):
        if infile.ilookup[field_no+i][ITEM_CODE] != code:
            sys.exit(f"Missing tiled fields {field_no} {code}")
        fields_list.append(infile.readfld(field_no+i))
    return np.array(fields_list)


# Parse command line arguments
parser = argparse.ArgumentParser(description="Update new PFT")
parser.add_argument('-i', '--input',  dest='ifile', help='Input UM dump')
parser.add_argument('-o', '--output', dest='ofile', help='Output UM dump')
parser.add_argument('-v', '--verbose',
        dest='verbose',
        action='store_true',
        default=False,
        help='verbose output',
        )
args = parser.parse_args()

# Open input file
input_file = umfile.UMFile(args.ifile)
nfields = input_file.fixhd[FH_LookupSize2]

# open output file
output_file = umfile.UMFile(args.ofile, "w")
output_file.copyheader(input_file)

# Read the file first to get the tiled data
tile_data = {}
field_no = 0
while field_no<nfields:
    ilookup = input_file.ilookup[field_no]
    if ilookup[LBEGIN]==-99: break # End of restart file.
    code = ilookup[ITEM_CODE]
    if code in TILED_CODES:
        if args.verbose: print("Reading code: ", code)
        tile_data[code] = read_tiles(input_file, field_no, ilookup[ITEM_CODE])
        field_no += NTILES
    else:
        field_no += 1

# Save 0s.
zeros = tile_data[FRACTIONS_CODE][BLANK_PFT].copy()

# What do I want to do witht he new PFT?
# Try just one grid cell
use_single_cell = False
LUCKY_CELL_Y, LUCKY_CELL_X = (85,41) #(y,x)

# I could simply transfer the crops values to the new PFT...
if use_single_cell:
    for code in TILED_CODES:
        tile_data[code][BLANK_PFT,LUCKY_CELL_Y,LUCKY_CELL_X] = \
                tile_data[code][CROP_PFT,LUCKY_CELL_Y,LUCKY_CELL_X]
    # Cover fractions
    # For now, I'm replacing crops with the new pft just to keep the fractions adding to 1
    tile_data[FRACTIONS_CODE][CROP_PFT,LUCKY_CELL_Y,LUCKY_CELL_X] = zeros[LUCKY_CELL_Y,LUCKY_CELL_X]
    # Set the previous year's cover fraction to the new cover fraction so there is no land-use change.
    tile_data[PREVIOUS_YEAR_FRACTIONS_CODE] = tile_data[FRACTIONS_CODE]
else:
    for code in TILED_CODES:
        tile_data[code][BLANK_PFT] = tile_data[code][CROP_PFT]
    # Cover fractions
    # For now, I'm replacing crops with the new pft just to keep the fractions adding to 1
    tile_data[FRACTIONS_CODE][CROP_PFT] = zeros
    # Set the previous year's cover fraction to the new cover fraction so there is no land-use change.
    tile_data[PREVIOUS_YEAR_FRACTIONS_CODE] = tile_data[FRACTIONS_CODE]

# Write to file
field_no = 0
while field_no<nfields:
    ilookup = input_file.ilookup[field_no]
    lbegin = ilookup[LBEGIN]
    if lbegin==-99:
        break
    code = ilookup[ITEM_CODE]

    if code in TILED_CODES:
        # Write the tiled output
        for i in range(NTILES):
            output_file.writefld(tile_data[code][i], field_no+i)
        field_no += NTILES
    else:
        if code==MASK_CODE:
            # Save the mask (needed for compression)
            field = input_file.readfld(field_no)
            output_file.writefld(field, field_no)
            output_file.mask = field
        else:
            # Copy from input file
            field = input_file.readfld(field_no, raw=True)
            output_file.writefld(field, field_no, raw=True)
        field_no += 1

output_file.close()

