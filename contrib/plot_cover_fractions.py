#!/usr/bin/env python3
"""Plot the cover fractions for CABLE land surface tiles.
"""
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np
import ipdb

#COVER_FRACTIONS_FILE = '/g/data/p66/txz599/data/luc_hist_maps/cableCMIP6_LC_1850.nc'
COVER_FRACTIONS_FILE = '1850_ACCESS-ESM_frac_IGBP_with_NVIS_in_Aus.nc'
READ_ONLY = 'r'
TILES = {
        0:'Evergreen needle leaf',
        1:'Evergreen broad leaf',
        2:'Deciduous needle leaf',
        3:'Deciduous broad leaf',
        4:'Shrub',
        5:'C3 grass',
        6:'C4 grass',
        7:'Tundra',
        8:'C3 crop',
        9:'C4 crop',
        10:'Wetland',
        11:'Arid shrub',
        12:'Eucalyptus',
        13:'Barren',
        14:'Urban',
        15:'Lakes',
        16:'Ice',
        }

# Load the cover fractions data.
ncfile = nc.Dataset(COVER_FRACTIONS_FILE, READ_ONLY)
fractions = ncfile.variables['frac'][:].squeeze()
lats = ncfile.variables['lat'][:]
lons = ncfile.variables['lon'][:]
ncfile.close()

# Create the figures
for tile in TILES.keys():
    plt.figure()
    plt.pcolormesh(lons, lats, fractions[tile,...], vmin=0, vmax=1)
    plt.colorbar()
    plt.title(TILES[tile])

#fig = plt.figure()
#gs = fig.add_gridspec(6, 3)
#n = 1
#for i in range(6):
#    for j in range(3):
#        if n==17: continue
#        ax = fig.add_subplot(gs[i,j], projection=ccrs.PlateCarree())
#        ax.pcolormesh(lons, lats, fractions[n,...], transform=ccrs.PlateCarree())
#        ax.set_title(TILES[n])
#        n += 1
#fig.subplots_adjust(
#        bottom=0.1,
#        top=0.9,
#        left=0.1,
#        right=0.9,
#        wspace=0.02,
#        hspace=0.1,
#        )

# Show that the fraction add up to 1.
plt.figure()
ax = plt.axes(projection=ccrs.PlateCarree())
shading = ax.pcolormesh(
        lons,
        lats,
        np.sum(fractions, axis=0),
        transform=ccrs.PlateCarree(),
        )
ax.coastlines()
plt.colorbar(shading, orientation='horizontal', pad=0.05)
plt.show()

